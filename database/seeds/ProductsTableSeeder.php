DB: table<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	for($i=0;$i<25;$i++){
        DB::table('products')->insert([
               'name' => str_random(30),
               'price' => rand(100,1000)
        	]);
         }
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {

    Route::get('/', function () {
        return view('welcome');
    });
    Route::get('success', function () {
        return "YOU HAVE BEEN REGISTERED";
    });
    Route::get('login', array('uses' => 'UserController@showLogin'));

    Route::post('login', array('uses' => 'UserController@doLogin' , 'as'=>'login'));

    Route::get('register', array('uses' => 'UserController@showRegister'));

    Route::post('register', array('uses' => 'UserController@doRegister' , 'as'=>'register'));



    Route::get('product',['uses'=>'ProductController@index']);

});

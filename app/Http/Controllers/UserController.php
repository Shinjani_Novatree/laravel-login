<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Hash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function showLogin()
    {
      return view('login');
    }

    public function doLogin()
    {

    }

    public function showRegister()
    {
        return view('registration');
    }

    public function doRegister()
    {
       /*rules for validation*/
        $rules = array(
            'email' => 'required|email|unique:users',
            'name' => 'required|alpha',
            'password' => 'required|min:2',
            'confirm_password' => 'required|same:password'
        );

        $validator = Validator::make(Input::all(),$rules);
        if($validator->fails()){
           return Redirect::to(URL::previous())->withInput()->withErrors($validator);
        }
        else{
            $user = new User();
            $user->name = Input::get('name');
            $user->email = Input::get('email');
            $user->password = Hash::make(Input::get('password'));
            $user->save();
            return Redirect::to('success');
        }
    }

}
